pipeline {
    agent {
        docker { image 'python' }
    }
    post {
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
    }
    options {
        gitLabConnection('gitlab')
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    stages {
        stage('Build Environment') {
            steps {
                sh 'pip install -r requirements.txt'
            }
        }
        stage('Static code metrics') {
            steps {
                echo 'Style check'
                sh 'pylint -d C0301 main.py transform.py'
                echo 'Code Coverage'
                sh 'coverage run -m unittest discover'
                sh 'python -m coverage xml -o reports/coverage.xml'
            }
            post {
                always {
                    step([
                        $class: 'CoberturaPublisher',
                        autoUpdateHealth: false,
                        autoUpdateStability: false,
                        coberturaReportFile: 'reports/coverage.xml',
                        failNoReports: false,
                        failUnhealthy: false,
                        failUnstable: false,
                        maxNumberOfBuilds: 10,
                        onlyStable: false,
                        sourceEncoding: 'ASCII',
                        zoomCoverageChart: false
                    ])
                }
            }
        }
        stage('Unit tests') {
            steps {
                //sh 'python test.py'
                sh 'py.test --junitxml test-reports/results.xml test.py'
            }
            post {
                always {
                    // Archive unit tests for the future
                    junit allowEmptyResults: true, testResults: 'test-reports/*.xml'
                }
            }
        }
    }
}